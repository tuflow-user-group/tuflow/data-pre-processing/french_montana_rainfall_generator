"""
Model exported as python.
Name : French Montana Rainfall Generator
Group : 
With QGIS : 33202
"""

from qgis.core import QgsProcessing
from qgis.core import QgsProcessingParameterNumber
from PyQt5.QtCore import QCoreApplication
from qgis._core import (QgsProcessingAlgorithm, QgsProcessingParameterFile, QgsProcessingParameterEnum,
                        QgsProcessingParameterBoolean, QgsProcessingParameterDefinition, QgsProcessingContext,
                        QgsProcessingFeedback, QgsProcessingMultiStepFeedback, QgsProcessingParameterString)
                        
from PyQt5.QtWidgets import QListView, QLabel, QTextBrowser, QToolButton, QLineEdit, QFileDialog
import processing
from processing.gui.AlgorithmDialog import AlgorithmDialog
import numpy as np
from matplotlib import pyplot as plt
from pathlib import Path
import os
import math
import typing


PARAMETERS = [
    'storm_duration_hours', 'position_of_peak', 'montana_parameter_a', 'montana_parameter_b', 'hyetograph_shape',
    'duration_of_intense_rainfall', 'output_location']
    
class French_Montana_Rainfall_Generator(QgsProcessingAlgorithm):

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterNumber('storm_duration_hours', 'Storm Duration (hours)',
                                                       type=QgsProcessingParameterNumber.Double, minValue=0.01, maxValue=24,
                                                       defaultValue=2))
        self.addParameter(QgsProcessingParameterNumber('position_of_peak', 'Position of Peak',
                                                       type=QgsProcessingParameterNumber.Double, minValue=0.01, maxValue=24, defaultValue=1))
        self.addParameter(QgsProcessingParameterNumber('montana_parameter_a', "Montana Parameter 'a'",
                                                       type=QgsProcessingParameterNumber.Double, minValue=0, defaultValue=None))
        self.addParameter(QgsProcessingParameterNumber('montana_parameter_b', "Montana parameter 'b'",
                                                       type=QgsProcessingParameterNumber.Double, minValue=0, maxValue=1, defaultValue=None))
        self.addParameter(QgsProcessingParameterEnum('hyetograph_shape', 'Hyetograph Shape', options=['Triangle','Double Triangle'], allowMultiple=False, usesStaticStrings=False, defaultValue=1))
        self.addParameter(QgsProcessingParameterNumber('duration_of_intense_rainfall', 'Duration of Intense Rainfall (hours)   **used for Double Triangle Hyetograph only', type=QgsProcessingParameterNumber.Double, minValue=0.001, maxValue=1, defaultValue=0.5))
        self.addParameter(QgsProcessingParameterFile('output_location', 'Output Directory', behavior=QgsProcessingParameterFile.Folder, fileFilter='All files (*.*)', defaultValue=None))

    def processAlgorithm(self, parameters, context, model_feedback):
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(1, model_feedback)
        feedback.setCurrentStep(0)
        results = {}
        outputs = {}
        storm_dur=parameters['storm_duration_hours']
        intense_dur=parameters['duration_of_intense_rainfall']
        pos_peak=parameters['position_of_peak']
        a=parameters['montana_parameter_a']
        b=parameters['montana_parameter_b']
        hyetograph_shape=parameters['hyetograph_shape']
        output_location=parameters['output_location']
        
        # Recursive function to return gcd
        # of a and b
        def gcd(a, b):
            if (a < b):
                return gcd(b, a)

            # base case
            if (abs(b) < 0.001):
                return a
            else:
                return (gcd(b, a - math.floor(a / b) * b))

        if hyetograph_shape==1:
            timestep=(gcd(storm_dur, intense_dur/2))
        else:
            timestep=(gcd(storm_dur, pos_peak))    
                
        output = f'{output_location}\\montana_rainfall.csv'
        bc_dbase = f'{output_location}\\bc_dbase.csv'
        
        output_=Path(output)
        bc_dbase_=Path(bc_dbase)

        #   Create Numpy Array of Rainfall Time
        rf_time=np.arange(0, storm_dur+timestep, timestep)

        # Calculate Time Points and Various Rainfall Intensities
        if hyetograph_shape==1: # Do for Double Triangle Hyetograph
            if pos_peak+(intense_dur/2)>=storm_dur: # Check that period of intense rainfall does not extend further than storm duration
                feedback.reportError('Error: The Period of Intense Rainfall extends further than Storm Duration.  Consider changing the storm duration, position of peak rainfall or the duration of intense rainfall')
            elif pos_peak-(intense_dur/2)<=0: # Check that period of intense rainfall does not occur before the start of the event
                feedback.reportError('Error: The Period of Intense Rainfall occurs before the start of the event.  Consider changing the position of peak rainfall or the duration of intense rainfall')
            elif storm_dur<4*intense_dur: # Check that period of intense rainfall is 4 times less than the storm duation*
                feedback.reportError('Error: The storm duration is less than 4 times the period of intense rainfall.  Consider changing the duration of intense rainfall or the storm duration')    
            else:
                r=intense_dur/storm_dur
                im=(2*a)*(2**-b)*((intense_dur/2)**-b)*((1-(r**(1-b)))/(((1-r)*(r**-b))))
                iM=(2*a)*(2**-b)*((intense_dur/2)**-b)*(((r**-b)-1)/((1-r)*(r**-b)))
                # Calculate Double Triangle Hyetograph Time point and Rainfall Intensities
                double_triangle_t =np.array([0,(pos_peak-intense_dur/2),pos_peak, (pos_peak+intense_dur/2), storm_dur])
                double_triangle_dep=np.array([0,im*timestep,iM*timestep,im*timestep,0])
                # Loop through times and determine rainfall depths based on double triangle method.  Time 0 rf_depth=0, abs-Peak_pos, peak rainfall (iM), at edge of intense rainfall period, rainfall = im,
                # otherwise interpolated from those known points.  This works with TUFLOW's stepped boundary approach.
                rf_depth = np.empty(len(rf_time))
                for i in range(len(rf_time)):
                    if rf_time[i] == 0:
                        rf_depth[i] =0
                    elif rf_time[i] == pos_peak:
                        rf_depth[i]=iM*timestep
                    elif rf_time[i] == pos_peak-(intense_dur/2):
                        rf_depth[i] = im*timestep
                    elif rf_time[i]== pos_peak+(intense_dur/2):
                        rf_depth[i] = im*timestep
                    elif rf_time[i] == storm_dur:
                        rf_depth[i] =0
                    elif rf_time[i] < pos_peak and rf_time[i]>pos_peak-(intense_dur/2):
                        rf_depth[i]= np.interp(rf_time[i],double_triangle_t, double_triangle_dep)
                    elif rf_time[i] > pos_peak and rf_time[i]<pos_peak+(intense_dur/2):
                        rf_depth[i]= np.interp(rf_time[i],double_triangle_t, double_triangle_dep)
                    elif rf_time[i] < pos_peak-(intense_dur/2):
                        rf_depth[i]= np.interp(rf_time[i],double_triangle_t, double_triangle_dep)
                    elif rf_time[i] > pos_peak+intense_dur/2:
                        rf_depth[i]= np.interp(rf_time[i],double_triangle_t, double_triangle_dep)
                with output_.open('w') as fo:
                    fo.write('Time (hrs),Rainfall (mm)\n')
                    for i in range(len(rf_time)):
                        fo.write(f'{rf_time[i]},{rf_depth[i]}\n')
        else:
            if pos_peak>=storm_dur:
                feedback.reportError('Error: The Peak Rainfall position extends further than Storm Duration.  Consider changing the position of peak rainfall or the duration or storm duration')
            elif pos_peak<=0:
                feedback.reportError('Error: The Peak Rainfall position occurs before the start of the event.  Consider changing the position of peak rainfall position')
            else:
                iM_single=2*(a*(storm_dur**-b)) # Do for Single Triangle Hyetograph
                # Calculate Single Triangle Hyetograph Time point and Rainfall Intensities
                single_triangle_t = np.array([0, pos_peak, storm_dur])
                single_triangle_dep = np.array([0, iM_single*timestep,0])
                # Loop through times and determine rainfall depths based on single triangle method.  Time 0 rf_depth=0, abs-Peak_pos, peak rainfall (iM), at edge of intense rainfall period, rainfall = im,
                # otherwise interpolated from those known points. This works with TUFLOW's stepped boundary approach.
                single_rf_depth = np.empty(len(rf_time))
                for i in range(len(rf_time)):
                    if rf_time[i] == 0:
                        single_rf_depth[i] =0
                    elif rf_time[i] == pos_peak:
                        single_rf_depth[i]=iM_single*timestep
                    elif rf_time[i] < pos_peak:
                        single_rf_depth[i]= np.interp(rf_time[i],single_triangle_t, single_triangle_dep)
                    elif rf_time[i] == storm_dur:
                        single_rf_depth[i] =0
                    elif rf_time[i] > pos_peak and rf_time[1] is not 0 :
                        single_rf_depth[i]= np.interp(rf_time[i],single_triangle_t, single_triangle_dep)
                    with output_.open('w') as fo:
                        fo.write('Time (hrs),Rainfall (mm)\n')
                        for i in range(len(rf_time)):
                            fo.write(f'{rf_time[i]},{single_rf_depth[i]}\n')
        
        # Check for errors before writing files
        if hyetograph_shape==1: 
            if pos_peak+(intense_dur/2)>=storm_dur:
                feedback.reportError('Errors Occurred: Consult previous error messages')
            elif pos_peak-(intense_dur/2)<=0:
                feedback.reportError('Errors Occurred: Consult previous error messages')
            elif storm_dur<4*intense_dur: # Check that period of intense rainfall is 4 times less than the storm duation*
                feedback.reportError('Errors Occurred: Consult previous error messages')   
            else:
                with bc_dbase_.open('w') as fo:
                    fo.write('Name,Source, Column 1, Column 2, Add Col 1, Mult Col 1, Mult Col 2, Add Col 2, Column 3, Column 3\n Montana_Rainfall, montana_rainfall.csv, Time (hrs), Rainfall (mm)\n')
                    feedback.pushInfo('Writing TUFLOW bc_dbase files...')
                    feedback.setCurrentStep(1)
        else: 
            if pos_peak>=storm_dur:
                feedback.reportError('Errors Occurred: Consult previous error messages')
            elif pos_peak<=0:
                feedback.reportError('Errors Occurred: Consult previous error messages')  
            else:  
                with bc_dbase_.open('w') as fo:
                    fo.write('Name,Source, Column 1, Column 2, Add Col 1, Mult Col 1, Mult Col 2, Add Col 2, Column 3, Column 3\n Montana_Rainfall, montana_rainfall.csv, Time (hrs), Rainfall (mm)\n')
                    feedback.pushInfo('Writing TUFLOW bc_dbase files...')
                    feedback.setCurrentStep(1)
        return results

    def name(self):
        return 'French Montana Rainfall Generator'

    def displayName(self):
        return 'French Montana Rainfall Generator'

    def group(self):
        return ''

    def groupId(self):
        return ''

    def shortHelpString(self) -> typing.AnyStr:
        folder = Path(os.path.realpath(__file__)).parent
        help_filename = folder / 'help' / 'html' / 'French_Montana_Rainfall_Generator.html'
        return help_filename.open().read()

    def shortDescription(self) -> typing.AnyStr:
        return self.tr("Generate French Rainfall Based on Montana Model Parameters")

    def createInstance(self):
        return French_Montana_Rainfall_Generator()

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)
