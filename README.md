### Description

This QGIS processing tool is used to generate French Rainfall Hyetographs based on Montana model and using either the double-triangle or triangle shape hyetograph.  The outputs are in a format required for use with TUFLOW.

For more information on the Montana model plase see the following link: 
[Montana Formula (in French)](http://wikhydro.developpement-durable.gouv.fr/index.php/Montana_(formule_type)_(HU))

### Setup

Download the files, unzip and put in the QGIS processing scripts folder usually something like C:\Users\USERNAME\AppData\Roaming\QGIS\QGIS3\profiles\default\processing\scripts).  Then open QGIS and you should see a tool in the Processing toolboxes.  

### Parameters

* **Storm Duration (hours)**: Total Duration of the Storm Event (T3) in hours.  Usually 1-4 Hours.
* **Position of Peak (hours)**: The time of the peak rainfall depth in hours (T2).  
* **Montana Parameter 'a'**: The Montana a(T) parameter for the study area. Limited to greater than or equal to 0.
* **Montana Parameter 'b'**: The Montana b(T) parameter for the study area.  **NOTE**: 'b' is specified as a positive value.  Limited to between 0 and 1.
* **Hyeteograph Shape**: User Specified Choice of Hyetograph Type.
Options are:
	* **Triangle**: Results in a single symmetrical triangular hyetograph. (for more information see [here (in French))](http://wiklimat.developpement-durable.gouv.fr/index.php/Pluie_de_projet_simple_triangle_(HU))
	* **Double Triangle**: Results in a symmetrical double triangular hyetograph with a period of intense rainfall around the peak of duration T1. (for more information see [here (in French))](http://wiklimat.developpement-durable.gouv.fr/index.php/Pluie_de_projet_double_triangle_(HU))
* **Duration of Intense Rainfall (hours)**: Period of intense rainfall for the double triangle hyetograph (T1). Usually 0.25-1 hour.  Limited to a maximum of 1/4 of the storm duration.
* **Output Directory**: Location to which the TUFLOW bc_dbase and generated rainfall hyeteograph time-series are to be saved to.
